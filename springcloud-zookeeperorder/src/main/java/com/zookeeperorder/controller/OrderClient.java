package com.zookeeperorder.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

/**
 * @Author: CPX
 * @Date: 2020/11/24 14:58
 * @version: 1.0
 */
@RestController
public class OrderClient {

    @Autowired
    private RestTemplate restTemplate;

    @RequestMapping("/getOrder")
    public String OrderClient(){
        String memberUrl = "http://zk-member/getMember";

        return restTemplate.getForObject(memberUrl, String.class);
    }
}
