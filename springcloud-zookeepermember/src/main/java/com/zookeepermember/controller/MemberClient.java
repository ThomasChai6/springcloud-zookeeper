package com.zookeepermember.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Author: CPX
 * @Date: 2020/11/24 15:02
 * @version: 1.0
 */
@RestController
public class MemberClient {

    @RequestMapping("/getMember")
    public String getMember(){
        return "this is member";
    }
}
