package com.zookeepermember;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@EnableDiscoveryClient
@SpringBootApplication
public class ZookeepermemberApplication {

	public static void main(String[] args) {
		SpringApplication.run(ZookeepermemberApplication.class, args);
	}

}
